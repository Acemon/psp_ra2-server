import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by Fernando on 15/02/2017.
 */
public class Cliente {
    public static final Logger logger = LogManager.getLogger(Cliente.class);
    private Socket socket;
    private String nick;
    PrintWriter salida;
    BufferedReader entrada;

    public Cliente(Socket socket) throws IOException {
        this.socket = socket;

        iniciar();
    }

    private void iniciar() throws IOException {
        entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        salida = new PrintWriter(socket.getOutputStream(), true);
    }

    public void desconectar() throws IOException {
        socket.close();
        logger.trace("Cliente desconectado");
    }

    public boolean isConectado(){
        return socket.isConnected();
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }
}
