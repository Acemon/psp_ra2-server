import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Fernando on 15/02/2017.
 */
public class ConexionCliente extends Thread{
    Servidor servidor;
    Cliente cliente;
    public static final Logger logger = LogManager.getLogger(ConexionCliente.class);

    public ConexionCliente(Servidor servidor, Cliente cliente) {
        this.servidor = servidor;
        this.cliente = cliente;
    }

    @Override
    public void run() {
        try{
            boolean acierto = false;
            String nombre = "";
            while (!acierto){
                String msn = cliente.entrada.readLine();
                String cachos[] = msn.split("#");
                if (cachos[0].equals("login")){
                    acierto = servidor.buscarBBDD(cachos[1], cachos[2]);
                }else if (cachos[0].equals("new")){
                    acierto = servidor.registrarBBDD(cachos[1], cachos[2]);
                }
                nombre = cachos[1];
                servidor.enviarLogin(acierto, cliente);
            }
            cliente.setNick(nombre);
            servidor.enviarConectados();
            servidor.enviar("Hola", cliente, "servidor");

            String mensaje= null;
            while (cliente.isConectado()){
                mensaje = servidor.recibir(cliente);
                logger.trace("Mensaje entrante");
                if (mensaje.startsWith("/")){
                    String [] partes = mensaje.split("#");
                    switch (partes[0]){
                        case "/user":
                            servidor.enviar(partes[2],servidor.buscarNick(partes[1]), cliente.getNick());
                            break;
                        case "/exit":
                            servidor.removeUser(cliente);
                            cliente.desconectar();
                            servidor.enviarConectados();
                            return;
                    }
                }else{
                    mensaje = cliente.getNick()+">"+mensaje;
                    servidor.enviarATodos(mensaje, cliente.getNick());
                }

            }
        }catch (IOException ioe){
            ioe.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
