import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Fernando on 15/02/2017.
 */
public class Servidor {
    public static final Logger logger = LogManager.getLogger(Servidor.class);
    private ServerSocket socket;
    private int puerto;
    private ArrayList<Cliente>clientes;
    private Connection conne;

    public void iniciar() throws IOException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        clientes = new ArrayList<>();
        socket = new ServerSocket(puerto);
        iniciarBBDD();
    }

    private void iniciarBBDD() throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        conne = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/mensajeria",
                "root",
                "");
        logger.trace("conexion bbdd iniciada");
    }

    public Servidor(int puerto) {
        this.puerto = puerto;
    }

    public void desconectar() throws IOException {
        socket.close();
    }

    public void anadirCliente(Cliente cliete) {
        clientes.add(cliete);
        logger.trace("cliente añadido");
    }

    public Socket aceptarConexion() throws IOException {
        return socket.accept();
    }

    public boolean isconectado() {
        if (socket != null)
            return !socket.isClosed();
        return false;
    }

    public void enviar (String mensaje, Cliente cliente, String user){
        cliente.salida.println("/msn#"+user+"#"+mensaje);
    }

    public void enviarAct(String mensaje, Cliente cliente){
        cliente.salida.println(mensaje);
    }

    public String recibir (Cliente cliente) throws IOException {
            return cliente.entrada.readLine();
    }

    public void enviarATodos(String mensaje, String nick){
        for (Cliente cliente : clientes){
            if (!cliente.getNick().equalsIgnoreCase(nick))
                enviarAct(mensaje, cliente);
        }
    }

    public void enviarConectados(){
        StringBuilder sb = new StringBuilder();
        sb.append("/nicks");
        for (Cliente cliente : clientes){
                sb.append("#");
                sb.append(cliente.getNick());
        }

        for (Cliente cliente : clientes){
            enviarAct(sb.toString(), cliente);
        }
    }

    public Cliente buscarNick(String parte) {
        for (Cliente cliente : clientes){
            if (cliente.getNick().equalsIgnoreCase(parte)){
                return cliente;
            }
        }
        return null;
    }

    public void removeUser(Cliente cliente) {
        clientes.remove(cliente);
        logger.trace("usuario eliminado");
    }

    public boolean buscarBBDD(String cacho, String cacho1) throws SQLException {
        String sentenciasql = "SELECT * FROM usuario WHERE usuario = ? AND pass = ?";
        PreparedStatement sentencia = conne.prepareStatement(sentenciasql);
        sentencia.setString(1, cacho);
        sentencia.setString(2, cacho1);
        ResultSet rs = sentencia.executeQuery();
        boolean ok = false;
        while (rs.next()){
            ok = true;
        }
        sentencia.close();
        rs.close();
        return ok;
    }

    public boolean registrarBBDD(String cacho, String cacho1) throws SQLException {
        String sentenciasql = "SELECT * FROM usuario WHERE usuario = ?";
        PreparedStatement sentencia = conne.prepareStatement(sentenciasql);
        sentencia.setString(1, cacho);
        ResultSet rs = sentencia.executeQuery();
        while (rs.next()){
            return false;
        }

        String sentenciasql2 = "INSERT INTO usuario (usuario, pass) VALUES (?, ?)";
        PreparedStatement sentencia2 = conne.prepareStatement(sentenciasql2);
        sentencia2.setString(1, cacho);
        sentencia2.setString(2, cacho1);
        sentencia2.execute();
        sentencia2.close();
        return true;
    }

    public void enviarLogin(boolean acierto, Cliente cliente) {
        cliente.salida.println("/login#"+acierto);
    }
}
