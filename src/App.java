import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.Socket;
import java.sql.SQLException;

/**
 * Created by Fernando on 15/02/2017.
 */
public class App {
    public static final Logger logger = LogManager.getLogger(App.class);
    public static void main (String[] args){
        Servidor servidor = new Servidor(5555);
        try{
            servidor.iniciar();
            logger.trace("App iniciada");
            while (servidor.isconectado()){
                Socket socket = servidor.aceptarConexion();
                Cliente cliente = new Cliente(socket);
                servidor.anadirCliente(cliente);
                ConexionCliente conexionCliente = new ConexionCliente(servidor, cliente);
                conexionCliente.start();
                logger.trace("Usuario admitido");
            }
        }catch (IOException ioe){
        } catch (IllegalAccessException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            logger.error(sw.toString());
        } catch (InstantiationException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            logger.error(sw.toString());
        } catch (SQLException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            logger.error(sw.toString());
        } catch (ClassNotFoundException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            logger.error(sw.toString());
        }
    }
}
